package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) 
	{
		BufferedReader br = null;
		misPeliculas = new ListaDobleEncadenada<VOPelicula>();
		peliculasAgno = new ListaEncadenada<VOAgnoPelicula>();
		try
		{
			br = new BufferedReader(new FileReader(archivoPeliculas));
			br.readLine();
			for(String linea = br.readLine(); linea != null; linea = br.readLine())
			{
				String[] datosPelicula = leerPelicula(linea);
				VOPelicula pelicula = new VOPelicula(datosPelicula[1], Integer.parseInt(datosPelicula[2]), darGeneros(datosPelicula[3]));
				misPeliculas.agregarElementoFinal(pelicula);
				agregarPeliculaAnio(peliculasAgno,pelicula);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(br != null)
					br.close();
			}
			catch(Exception e)
			{e.printStackTrace();}
		}
	}

	private void agregarPeliculaAnio(ILista<VOAgnoPelicula> lista, VOPelicula peli)
	{		
		for (int i = 0; i < lista.darNumeroElementos(); i++) 
		{
			if(lista.darElemento(i).getAgno() == peli.getAgnoPublicacion())
			{
				lista.darElemento(i).getPeliculas().agregarElementoFinal(peli);
				return;
			}
		}
		ListaDobleEncadenada<VOPelicula> peliculasAgno = new ListaDobleEncadenada<>();
		peliculasAgno.agregarElementoFinal(peli);
		lista.agregarElementoFinal(new VOAgnoPelicula(peli.getAgnoPublicacion(), peliculasAgno));
	}

	private ILista<String> darGeneros(String pLinea)
	{
		String linea = pLinea;
		ListaEncadenada<String> lista = new ListaEncadenada<String>();
		char[] as = linea.toCharArray();
		String acom = "";
		for (int i = 0; i < as.length; i++) 
		{
			while (i < as.length && as[i] != '|' ) 
			{
				acom += Character.toString(as[i]);
				i++;
			}
			lista.agregarElementoFinal(acom);
			acom = "";
		}
		return lista;
	}

	private String[] leerPelicula(String linea)
	{
		String[] pelicula = linea.split(",");
		if(pelicula.length > 3)

		{
			String nombre = "";
			for(int i = 1; i < pelicula.length -1 ; i++)
				nombre += (i==1)? pelicula[i] : ","+ pelicula[i];
				pelicula = new String[]{pelicula[0], nombre.replaceAll("\"", ""), pelicula[pelicula.length-1]};
		}
		String[] anioSeparado = separarAnio(pelicula[1]);
		pelicula = new String[]{pelicula[0], anioSeparado[0], anioSeparado[1] , pelicula[2]};//separarAnio(pelicula[1]), pelicula[2]};
		return pelicula;
	}

	private String[] separarAnio(String linea)
	{
		int longitud = linea.length();
		String ret = linea.substring(longitud-5, longitud-1);
		try
		{
			Integer.parseInt(ret);
			return new String[]{linea.substring(0, longitud-6), ret};
		}
		catch (NumberFormatException nfe)
		{
			return new String[]{linea,"1950"};
		}
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) 
	{
		ListaEncadenada<VOPelicula> ret = new ListaEncadenada<VOPelicula>();
		for(VOPelicula pelicula : misPeliculas)
			if(pelicula.getTitulo().contains(busqueda))
				ret.agregarElementoFinal(pelicula);
		return ret;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) 
	{
		ILista<VOPelicula>ret = new ListaEncadenada<VOPelicula>();
		for(VOAgnoPelicula peliculas : peliculasAgno){
			if(peliculas.getAgno() == agno){
				if(ret==null){ret =peliculas.getPeliculas();}
				else{
					for (VOPelicula p:peliculas.getPeliculas()) {
						ret.agregarElementoFinal(p);
					}

				}
			}

		}
		return ret;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() 
	{
		peliculasAgno.avanzarSiguientePosicion();
		return peliculasAgno.darElementoPosicionActual();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() 
	{
		peliculasAgno.retrocederPosicionAnterior();
		return peliculasAgno.darElementoPosicionActual();
	}

}

package model.vo;

import model.data_structures.ILista;

public class VOAgnoPelicula {

	private int agno;
	private ILista<VOPelicula> peliculas;
	
	public VOAgnoPelicula(int agno, ILista<VOPelicula> peliculas) {
		this.agno = agno;
		this.peliculas = peliculas;
	}
	public int getAgno() {
		return agno;
	}
	public void setAgno(int agno) {
		this.agno = agno;
	}
	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}
	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}
	
	@Override
	public String toString() {
		return "A�o: "+ agno + " Cantidad peliculas: "+ peliculas.darNumeroElementos();
	}
	
}

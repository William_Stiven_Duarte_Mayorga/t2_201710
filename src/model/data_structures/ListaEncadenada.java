package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> 
{
	private NodoSimple<T> primero;
	private NodoSimple<T> actual;
	private NodoSimple<T> ultimo;
	private int tamanio;

	@Override
	public void agregarElementoFinal(T elem) 
	{
		if(primero == null)
		{
			primero = new NodoSimple<T>(elem);
			actual = primero;
			ultimo = primero;
		}
		else
		{
			ultimo.setNext(new NodoSimple<T>(elem));
			ultimo = ultimo.getNext();
		}
		tamanio++;
	}

	@Override
	public T darElemento(int pos) 
	{
		if(pos < 0 || pos > tamanio-1)
			throw new IndexOutOfBoundsException("Indice fuera del intervalo");
		else if(pos == 0)
			return primero.getItem();
		else if(pos == tamanio-1)
			return ultimo.getItem();
		else
			return primero.get(pos);
	}


	@Override
	public int darNumeroElementos() 
	{
		return tamanio;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		return actual.getItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual.getNext() == null)
			return false;
		actual = actual.getNext();
		return true;
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		NodoSimple<T> temp = primero;
		if(temp.equals(actual))
			return false;
		while( temp.getNext() != null)
		{
			if(temp.getNext().equals(actual))
			{
				actual = temp;
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			
			private NodoSimple<T> nodo = primero;
			
			@Override
			public boolean hasNext() 
			{
				return nodo != null && nodo.getNext() != null;
			}

			@Override
			public T next() 
			{
				NodoSimple<T> temp = nodo;
				nodo = nodo.getNext();
				return temp.getItem();
			}

			@Override
			public void remove() 
			{}
		};
	}
}

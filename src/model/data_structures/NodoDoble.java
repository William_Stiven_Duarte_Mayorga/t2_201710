package model.data_structures;

public class NodoDoble<T> 
{
	private T item;
	private NodoDoble<T> next, pre;
	
	public NodoDoble(T item)
	{
		this.item = item;
		this.next = null;
		this.pre=null;
	}
	
	public NodoDoble(T item, NodoDoble<T> post, NodoDoble<T> pre)
	{
		this.item = item;
		this.next = post;
		this.pre=pre;
	}
	
	public T getItem() { return item; }

	public NodoDoble<T> getNext() { return next; }

	public NodoDoble<T> getPre() { return pre; }
	
	public void setItem(T item) { this.item = item;}
	
	public void setNext(NodoDoble<T> next) { this.next = next; }

	public void setPre(NodoDoble<T> pre) { this.pre = pre; }
	
	public T get(int pos)
	{
		if(pos == 0)
			return this.item;
		else
			return item;
	}


}

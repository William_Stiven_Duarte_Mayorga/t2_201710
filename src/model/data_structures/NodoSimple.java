package model.data_structures;

public class NodoSimple<T> 
{
	private T item;
	private NodoSimple<T> next;
	
	public NodoSimple(T item)
	{
		this.item = item;
		this.next = null;
	}
	
	public NodoSimple(T item, NodoSimple<T> nodo)
	{
		this.item = item;
		this.next = nodo;
	}
	
	public T getItem() 
	{ return item; }
	
	public NodoSimple<T> getNext() 
	{ return next; }
	
	public void setItem(T item) 
	{ this.item = item; }
	
	public void setNext(NodoSimple<T> next) 
	{ this.next = next; }
	
	public T get(int pos)
	{
		if(pos == 0)
			return this.item;
		else
			return next.get(pos - 1);
	}

}

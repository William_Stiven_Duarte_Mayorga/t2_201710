package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> start,cursor,end;
	private int size=0;

	@Override
	public Iterator<T> iterator() {
		cursor = start;
		return new Iterator<T>() {
			
			@Override
			public boolean hasNext() {
				return cursor!=null;
			}
			@Override
			public T next(){
				NodoDoble<T> a=cursor;
				cursor=a.getNext();
				return a.getItem();
				}
		};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(start==null){
			start=new NodoDoble<T>(elem);
			end=start;
		}else{
			NodoDoble<T> n=new NodoDoble<T>(elem,null,end);
			end.setNext(n);
			end=end.getNext();
		}
		 size++;
	}

	@Override
	public T darElemento(int pos) {
		if(pos < 0)
			throw new IndexOutOfBoundsException("Indice fuera del intervalo");
		else if(pos == 0)
			return start.getItem();
		return start.get(pos-1);
	}


	@Override
	public int darNumeroElementos() {
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		return (T) cursor;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		return (cursor.getNext()!=null);
	}

	@Override
	public boolean retrocederPosicionAnterior() {return (cursor.getPre()!=null);}
	
}

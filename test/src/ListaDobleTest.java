import java.awt.Cursor;
import java.util.Iterator;

import model.data_structures.*;
import junit.framework.TestCase;


public class ListaDobleTest<T> extends TestCase {

	private ListaDobleEncadenada<T> listadoble;

	private  void setupData(){
		try{
			listadoble=new ListaDobleEncadenada<>();
			for (int i=1;i<100;i++)
				listadoble.agregarElementoFinal((T) ("Data:"+String.valueOf(i)));
		}catch(Exception e){
			fail("Error de construccion");
		}
	}
	public void testListaDoble(){
		setupData();
		if(listadoble!=null){
			assertNotNull("La lista no tiene acceso al primer elemento",listadoble.darElemento(0));
		}else{
			fail("Lista vacia");
		}
	}
	
	public void testTterador() {
		setupData();
		
		Iterator<T> iterator=listadoble.iterator();
		NodoDoble<T> primer=((NodoDoble<T>)iterator.next());
		NodoDoble<T> otro=((NodoDoble<T>)iterator.next());
		assertTrue("El iterador no avanza en la lista",!otro.getItem().equals(primer.getItem()));
	}
	
	public void testAgregarElementoFinal() {
		setupData();
		T elem=(T)"sdd";
		int tam=listadoble.darNumeroElementos();
		listadoble.agregarElementoFinal(elem);
		assertTrue("No se adiciono el elemento",tam!=listadoble.darNumeroElementos());
		Iterator<T> iterator=listadoble.iterator();
		T otro=null;
		while(iterator.hasNext()){
			otro=((NodoDoble<T>)iterator.next()).getItem();
			
		}
		assertEquals("El elemento no se adiciono al final",otro,elem);
	}
	
	public void testDarElemento() {
		setupData();
		T e=listadoble.darElemento(1);
	 assertNotNull("Error dar elemento",e);
	
	 }
	
	public void testDarNumeroElementos() {
		setupData();
		int tam=listadoble.darNumeroElementos();
		assertEquals("El numero de elementos no es correcto",tam,99);
		listadoble.agregarElementoFinal((T)"Fogf");
		assertTrue("No se adiciono el elemento",tam!=listadoble.darNumeroElementos());
	}
	
	public void testDarElementoPosicionActual() {
			setupData();
			Iterator<T> c=listadoble.iterator();
			T e=listadoble.darElementoPosicionActual();
			T s=c.next();
			assertNotNull("Error dar elemento",e);
			assertEquals("No se extrajo el elemento correcto",s,e);
	}
	
	public void testAvanzarSiguientePosicion() {
		setupData();
		Iterator<T> iterator=listadoble.iterator();
		assertTrue("El elemento no es el siguiente",listadoble.avanzarSiguientePosicion());
	}
	
	public void testRetrocederPosicionAnterior() {
		setupData();
		T sig=listadoble.darElemento(1);
		assertTrue("El elemento no es el siguiente",sig.equals("Data:1"));
		
	}

}
